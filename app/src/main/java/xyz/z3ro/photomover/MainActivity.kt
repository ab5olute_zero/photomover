package xyz.z3ro.photomover

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import xyz.z3ro.photomover.utility.Constants
import xyz.z3ro.photomover.utility.FileUtilities

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var sharedPreferences: SharedPreferences? = null

    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_DENIED
        ) {
            askPermission()
        }

        button_move.setOnClickListener(this)
        button_permission.setOnClickListener(this)
        button_test.setOnClickListener(this)
    }

    private fun askPermission() {
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        ActivityCompat.requestPermissions(this, permissions, Constants.STORAGE_PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.STORAGE_PERMISSION_REQUEST_CODE -> if (isPermissionGranted(grantResults)) {

            } else
                dialogOnPermissionDenied()
        }
    }

    private fun isPermissionGranted(grantResults: IntArray): Boolean {
        if (grantResults.isEmpty())
            return false
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED)
                return false
        }
        return true
    }

    private fun dialogOnPermissionDenied() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Notice")
            .setMessage("Storage permission is denied! App won't work as it it supposed to.")
            .setPositiveButton("Ok") { dialogInterface, _ ->
                dialogInterface.dismiss()
                finish()
            }
            .setOnCancelListener { dialogInterface ->
                dialogInterface.dismiss()
                finish()
            }
            .show()
    }

    override fun onClick(view: View) {
        val fileUtilities = FileUtilities(this, this)
        when (view.id) {
            R.id.button_move -> {
                if (fileUtilities.isSourcePresent())
                    if (fileUtilities.isDestinationPresent())
                        fileUtilities.move()
                    else {
                        Snackbar.make(
                            findViewById(R.id.main_layout),
                            R.string.destination_not_found,
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                else
                    Toast.makeText(this, R.string.source_not_found, Toast.LENGTH_LONG).show()

            }
            R.id.button_permission -> {
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
                intent.addFlags(
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
                            or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                            or Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
                            or Intent.FLAG_GRANT_PREFIX_URI_PERMISSION
                )
                startActivityForResult(intent, Constants.EXTERNAL_STORAGE_ACTIVITY_CODE)
            }
            R.id.button_test -> {
                fileUtilities.createDummyFile()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.EXTERNAL_STORAGE_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data!!.data
            sharedPreferences!!.edit().putString(Constants.EXTERNAL_URI, uri!!.toString()).apply()

            val takeFlags: Int = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            contentResolver.takePersistableUriPermission(uri, takeFlags)
        }
    }
}