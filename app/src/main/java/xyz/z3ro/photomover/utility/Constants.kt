package xyz.z3ro.photomover.utility

object Constants {
    const val SHARED_PREF_FILE = "xyz.z3ro.photomover.PREFERENCE_FILE"
    const val EXTERNAL_URI = "EXTERNAL_URI"

    const val STORAGE_PERMISSION_REQUEST_CODE = 777
    const val EXTERNAL_STORAGE_ACTIVITY_CODE = 776
}
