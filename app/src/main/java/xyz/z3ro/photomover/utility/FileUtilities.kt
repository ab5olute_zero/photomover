package xyz.z3ro.photomover.utility

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Environment
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.documentfile.provider.DocumentFile
import xyz.z3ro.photomover.R
import java.io.*

class FileUtilities(val activity: Activity, val context: Context) {

    private val sourcePath: String?
        get() {
            val cameraPath = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera/")
            return if (cameraPath.exists()) cameraPath.absolutePath else null
        }
    private val destinationPath: String?
        get() {
            val sharedPreferences: SharedPreferences =
                activity.getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE)
            return sharedPreferences.getString(Constants.EXTERNAL_URI, null)
        }

    fun move() {
        val sourceDirectory = File(sourcePath)
        val destinationDirectory = DocumentFile.fromTreeUri(context, Uri.parse(destinationPath))
        copyFileOrDirectory(sourceDirectory, destinationDirectory, true)
    }

    private fun copyFileOrDirectory(source: File?, destination: DocumentFile?, isRootDirectory: Boolean) {
        if (source == null)
            return
        if (destination == null || !destination.exists()) {
            Toast.makeText(context, R.string.destination_not_found, Toast.LENGTH_LONG).show()
        } else {
            var destinationDirectory: DocumentFile? = null
            if (source.isDirectory) {
                if (!isRootDirectory) {
                    destinationDirectory = destination?.createDirectory(source.name)
                }
                source.list().forEach {
                    copyFileOrDirectory(File(source, it), destinationDirectory ?: destination, false)
                    /* Deleting directory after moving contents */
                    if (!isRootDirectory)
                        source.delete()
                }
            } else {
                val destinationFile = destination?.createFile(getMimeType(source.path)!!, source.name)
                copyFile(source, destinationFile)
            }
        }
    }

    private fun copyFile(sourceFile: File, destinationFile: DocumentFile?) {

        val inputStream: InputStream = FileInputStream(sourceFile.absolutePath)
        var outputStream: OutputStream? = null
        if (destinationFile != null) {
            try {
                outputStream = context.contentResolver.openOutputStream(destinationFile.uri)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        if (outputStream != null) {
            try {
                inputStream.copyTo(outputStream, 65536)
                inputStream.close()
                outputStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        /* Delete file after copying */
        sourceFile.delete()
    }

    private fun getMimeType(url: String): String? {
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        return mimeType ?: "*/*"
    }

    fun isSourcePresent(): Boolean {
        return File(sourcePath).exists()
    }

    fun isDestinationPresent(): Boolean {
        return DocumentFile.fromTreeUri(context, Uri.parse(destinationPath))?.exists() ?: false
    }

    fun createDummyFile() {
        val pickedDir = DocumentFile.fromTreeUri(context, Uri.parse(destinationPath))
        if (pickedDir == null || !pickedDir.exists()) {
            Toast.makeText(context, R.string.destination_not_found, Toast.LENGTH_LONG).show()
        }
        val newFile = pickedDir!!.createFile("check/check", ".check")
        var out: OutputStream? = null
        if (newFile != null) {
            try {
                out = context.contentResolver.openOutputStream(newFile.uri)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        if (out != null) {
            try {
                out.write(context.getString(R.string.check_string).toByteArray())
                out.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

//    class CopyTask : AsyncTask<Void, Void, Void>() {
//        override fun doInBackground(vararg p0: Void?): Void {
//
//            return
//        }
//    }
}